<?php
include "PDOConnectionFactory.php";

$factory = new PDOConnectionFactory('localhost','root','','cettictest');
$conexion = $factory->GetConnection();
$sql = "select * from variables_juego";
$statement = $conexion->prepare($sql);
$statement->execute();
$resultado = $statement->fetch(PDO::FETCH_ASSOC);

echo $resultado['velocidad'] . '-' . 
     $resultado['duracion_nivel'] . '-' . 
     $resultado['puntos_item'] . '-' . 
     $resultado['vitalidad'] . '-' . 
     $resultado['damage'];