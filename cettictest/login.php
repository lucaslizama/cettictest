<?php
include "PDOConnectionFactory.php";

if($_SERVER['REQUEST_METHOD'] !== "POST") {
    echo "Error";
    exit;
}

$user = $_POST['username'];
$pass = $_POST['password'];

try {
    $factory = new PDOConnectionFactory('localhost','root','','cettictest');
    $conexion = $factory->GetConnection();
    $sql = "select * from usuarios where username = ? ";
    $statement = $conexion->prepare($sql);
    $statement->bindParam('s', $user);
    
    if($statement->execute() == false) {
        echo "false";
        $conexion = null;
        exit;
    }

    $result = $statement->fetch(PDO::FETCH_ASSOC);

    if($result == null) {
        echo "false";
        $conexion = null;
        exit;
    }

    if(password_verify($pass, $result['password'])){
        echo "true";
        $conexion = null;
        exit;
    }

    $conexion = null;

} catch(PDOException $e) {
    echo "false";
    $conexion = null;
}