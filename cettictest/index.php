<?php
    include "PDOConnectionFactory.php";

    $factory = new PDOConnectionFactory('localhost','root','','cettictest');
    $conexion = $factory->GetConnection();
    $sql = "select * from variables_juego";
    $statement = $conexion->prepare($sql);
    $statement->execute();
    $resultado = $statement->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Variables</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <form action="actualizarVariables.php" method="post">
        <fieldset>
            <legend>Variables</legend>
            <label for="velocidad">Velocidad</label>
            <input type="number" name="velocidad" id="velocidad" value="<?=$resultado['velocidad']?>"> 
            <label for="duracion_nivel">Duracion Nivel</label>
            <input type="number" name="duracion_nivel" id="duracion_nivel" value="<?=$resultado['duracion_nivel']?>">
            <label for="puntos_item">Puntos por Item</label>
            <input type="number" name="puntos_item" id="puntos_item" value="<?=$resultado['puntos_item']?>">
            <label for="vitalidad">Vitalidad</label>
            <input type="number" name="vitalidad" id="vitalidad" value="<?=$resultado['vitalidad']?>">
            <label for="damage">Daño Enemigo</label>
            <input type="number" name="damage" id="damage" value="<?=$resultado['damage']?>">
            <button type="submit">Actualizar</button>
        </fieldset>
    </form>
</body>

<?php $conexion = null; ?>
</html>