<?php

class PDOConnectionFactory {
    private $host;
    private $user;
    private $pass;
    private $dbname;

    public function __construct($host,$user,$pass,$dbname)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->dbname = $dbname;
    }

    public function GetConnection() : PDO{
        $conexion = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
        return $conexion;
    }
}