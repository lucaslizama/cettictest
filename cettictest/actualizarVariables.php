<?php 
include "PDOConnectionFactory.php";

$velocidad = $_POST['velocidad'];
$duracion = $_POST['duracion_nivel'];
$puntos = $_POST['puntos_item'];
$vitalidad = $_POST['vitalidad'];
$damage = $_POST['damage'];

$factory = new PDOConnectionFactory('localhost','root','','cettictest');
$conexion = $factory->GetConnection();
$sql = "update variables_juego " .
        "set velocidad=:velocidad, duracion_nivel=:duracion, puntos_item=:puntos, vitalidad=:vitalidad, damage=:damage " . 
        "where id=1";

$statement = $conexion->prepare($sql);
$statement->bindParam(':velocidad', $velocidad);
$statement->bindParam(':duracion', $duracion);
$statement->bindParam(':puntos', $puntos);
$statement->bindParam(':vitalidad', $vitalidad);
$statement->bindParam(':damage', $damage);
$statement->execute();

$conexion = null;

header('Location: ' . 'http://localhost/cettictest');