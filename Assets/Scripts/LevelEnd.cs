﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;

/// <summary>
/// Clase que controla el fin del juego al terminar el nivel.
/// </summary>
public class LevelEnd : MonoBehaviour {
	public GameObject splashScreen;
	public Text texto;

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this
	/// object (2D physics only).
	/// </summary>
	/// <param name="other">The other Collider2D involved in this collision.</param>
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player")) {
			splashScreen.SetActive(true); //Mostrar Splash Screen
			other.gameObject.SetActive(false); //Desactivar al Player
			texto.text = "Terminaste el Nivel! \n Puntaje: " + GameManager.Instance.Points.ToString("000000");			
		}
	}

	public void QuitGame() {
		Application.Quit();
	}
}
