﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

/// <summary>
/// Clase que controla el timer del nivel y ejecuta un evento
/// al terminar el tiempo.
/// </summary>
public class LevelTimer : MonoBehaviour {

	public float segundos = 100f;
	public UnityEvent onTimerEnd;
	private Text texto;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		texto = GetComponent<Text>();
	}

	
	// Update is called once per frame
	void Update () {
		segundos -= Time.deltaTime;
		segundos = Mathf.Clamp(segundos, 0, 999);
		texto.text = "Time \n" +  Convert.ToInt32(segundos).ToString("000");
		
		if(segundos == 0f) {
			onTimerEnd.Invoke();
		}
	}
}
