﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

/// <summary>
/// Clase que contiene las funciones y referencias necesarias
/// para el funcionamiento del menu principal
/// </summary>
public class MainMenu : MonoBehaviour {
	public string gameScene;
	public InputField loginUsername;
	public InputField loginPassword;
	public InputField registerUsername;
	public InputField registerPassword;
	private string hostURL = "http://localhost:80/cettictest/";

	/// <summary>
	/// Metodo que inicia el proceso asincrono de validar el login.
	/// </summary>
	public void Login() {
		StartCoroutine(LoginRoutine());
	}

	/// <summary>
	/// Metodo que inicia el proceso asincrono de registrar un usuario.
	/// </summary>
	public void Register() {
		StartCoroutine(RegisterRoutine());
	}

	/// <summary>
	/// Corutina que valida el login del usuario en base 
	/// a los datos ingresados en los campos de username y password
	/// </summary>
	/// <returns></returns>
	private IEnumerator LoginRoutine() {
		if(loginUsername.text.Equals(string.Empty) || loginPassword.text.Equals(string.Empty)) {
			yield break;
		}

		ToggleLoginFieldsInteractability();

		WWWForm form = new WWWForm();
		form.AddField("username", loginUsername.text);
		form.AddField("password", loginPassword.text);
		var request = UnityWebRequest.Post(hostURL + "login.php", form);

		yield return request.SendWebRequest();

		string data = request.downloadHandler.text;

		if(data.Equals("false")) {
			ToggleLoginFieldsInteractability();
			yield break;
		} 
			
		SceneManager.LoadScene(gameScene);
	}	

	/// <summary>
	/// Corutina que registra un usuario
	/// </summary>
	/// <returns></returns>
	private IEnumerator RegisterRoutine() {
		if(registerUsername.text.Equals(string.Empty) || registerPassword.text.Equals(string.Empty)) {
			yield break;
		}

		ToggleRegisterFieldsInteractability();

		WWWForm form = new WWWForm();
		form.AddField("username", registerUsername.text);
		form.AddField("password", registerPassword.text);
		var request = UnityWebRequest.Post(hostURL + "register.php", form);

		yield return request.SendWebRequest();

		string data = request.downloadHandler.text;

		if(data.Equals("false"))
        {
            ToggleRegisterFieldsInteractability();
            yield break;
        }
        else {
			SceneManager.LoadScene(gameScene);
		}
	}

	/// <summary>
	/// Metodo que activa o desactiva la interactivilidad
	/// de los campos de username y password del formulario
	/// de registro
	/// </summary>
    private void ToggleRegisterFieldsInteractability()
    {
        registerUsername.interactable = true;
        registerPassword.interactable = true;
    }

	/// <summary>
	/// Metodo que activa o desactiva la interactivilidad
	/// de los campos de username y password del formulario
	/// de login
	/// </summary>
	private void ToggleLoginFieldsInteractability() {
		loginUsername.interactable = true;
		loginPassword.interactable = true;
	}
}
