﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using MoreMountains.CorgiEngine;

/// <summary>
/// Clase que recupera las 5 variables requeridas para la demo
/// desde el servidor y actualiza los objetos correspondientes en la
/// escena.
/// </summary>
public class GameVariablesRetriever : MonoBehaviour {
	public bool retrieveOnStart = true;
	public CharacterHorizontalMovement characterMovement;
	public Health characterHealth;
	public LevelTimer timeManager;
	public Coin[] collectables;
	public DamageOnTouch[] enemyDamages;
	private string hostURL = "http://localhost:80/cettictest/";

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		characterMovement = player.GetComponent<CharacterHorizontalMovement>();
		characterHealth = player.GetComponent<Health>();
	}

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		if(!retrieveOnStart) return;
		StartCoroutine(RetrieveVariablesRoutine());
	}

    /// <summary>
    /// Corutina que recupera las variables del servidor y actualiza
    /// los objetos de la escena.
    /// </summary>
    /// <returns></returns>
	private IEnumerator RetrieveVariablesRoutine()
    {
        var request = UnityWebRequest.Get(hostURL + "obtenerVariables.php");

        yield return request.SendWebRequest();

        string data = request.downloadHandler.text;
        string[] values = data.Split('-');

        //Actualizar Variables
        characterMovement.MovementSpeed = Convert.ToInt32(values[0]);
        timeManager.segundos = Convert.ToInt32(values[1]);
        UpdateCollectablePoints(values);
        characterHealth.InitialHealth = Convert.ToInt32(values[3]);
        UpdateEnemyDamage(values);
    }

    /// <summary>
    /// Metodo que actualiza el daño causado
    /// por cada enemigo de la escena.
    /// </summary>
    /// <param name="values"></param>
    private void UpdateEnemyDamage(string[] values)
    {
        foreach (DamageOnTouch enemy in enemyDamages)
        {
            enemy.DamageCaused = Convert.ToInt32(values[4]);
        }
    }

    /// <summary>
    /// Metodo que actualiza la cantidad de puntos entregados 
    /// por cada coleccionable del nivel.
    /// </summary>
    /// <param name="values"></param>
    private void UpdateCollectablePoints(string[] values)
    {
        foreach (Coin collectable in collectables)
        {
            collectable.PointsToAdd = Convert.ToInt32(values[2]);
        }
    }
}
